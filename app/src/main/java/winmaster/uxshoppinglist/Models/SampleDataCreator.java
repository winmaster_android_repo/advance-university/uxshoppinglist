package winmaster.uxshoppinglist.Models;

import java.sql.Date;

import io.realm.Realm;
import io.realm.RealmList;

public class SampleDataCreator
{
    public static void AddSampleData(Realm realm)
    {
        Product product1 = new Product("Banan", 3, new Date(System.currentTimeMillis()));
        Product product2 = new Product("Brokuł", 1, new Date(System.currentTimeMillis()));
        Product product3 = new Product("Chleb", 1, new Date(System.currentTimeMillis()));

        Product product4 = new Product("Papryka", 2, new Date(System.currentTimeMillis()));
        Product product5 = new Product("Pomidor", 1, new Date(System.currentTimeMillis()));
        Product product6 = new Product("Cebula", 3, new Date(System.currentTimeMillis()));

        RealmList<Product> list1 = new RealmList<>();
        list1.add(product1);
        list1.add(product2);
        list1.add(product3);

        ShoppingList shoppingList1 = new ShoppingList("Market", list1);

        RealmList<Product> list2 = new RealmList<>();
        list2.add(product4);
        list2.add(product5);
        list2.add(product6);

        ShoppingList shoppingList2 = new ShoppingList("Bazar", list2);

        realm.beginTransaction();
        realm.insertOrUpdate(shoppingList1);
        realm.insertOrUpdate(shoppingList2);
        realm.commitTransaction();
    }
}
