package winmaster.uxshoppinglist;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.sql.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import winmaster.uxshoppinglist.LayoutGenerators.CustomAddProductTile;
import winmaster.uxshoppinglist.Models.Product;
import winmaster.uxshoppinglist.Models.ShoppingList;

public class EditListActivity extends AppCompatActivity
{
    public Realm realm;
    String listName;
    ShoppingList shoppingList;
    EditText editTextListName;
    LinearLayout linearLayoutEditedProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_list);

        realm = Realm.getDefaultInstance();

        listName = getIntent().getExtras().getString("LIST_NAME");

        editTextListName = findViewById(R.id.editTextListName);
        editTextListName.setText(listName);

        linearLayoutEditedProducts = findViewById(R.id.linearLayoutEditedProducts);

        arrangeProductsToList();
    }

    private void arrangeProductsToList()
    {
        shoppingList = realm.where(ShoppingList.class).equalTo("name", listName).findFirst();
        List<Product> productList = shoppingList.getProducts();

        for( Product product: productList)
        {
            CustomAddProductTile customProductTile = new CustomAddProductTile(this);
            customProductTile.Builder(product.getName(), product.getAmount());

            linearLayoutEditedProducts.addView(customProductTile);
            linearLayoutEditedProducts.refreshDrawableState();
        }
    }

    public void deleteList(View view)
    {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(R.string.deleteEntry)
                .setMessage(R.string.deleteConfirm)
                .setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        realm.executeTransaction(new Realm.Transaction()
                        {
                            @Override
                            public void execute(Realm realm)
                            {
                                shoppingList.deleteFromRealm();
                            }
                        });

                        Toast.makeText(getApplicationContext(), getText(R.string.deleteSuccess), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                    }
                })
                .setNegativeButton(R.string.No, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .create();

        dialog.show();
    }

    public void buttonSaveListClicked(View view)
    {
        RealmList<Product> products = new RealmList<>();

        for(int i=0; i<linearLayoutEditedProducts.getChildCount(); i++)
        {
            CustomAddProductTile child = (CustomAddProductTile)linearLayoutEditedProducts.getChildAt(i);
            Product product = new Product(child.GetProductNameData(), child.GetNumberPickerData(), new Date(System.currentTimeMillis()));
            products.add(product);
        }

        ShoppingList editedShoppingList = new ShoppingList(editTextListName.getText().toString(), products);

        realm.beginTransaction();
        shoppingList.deleteFromRealm();
        realm.insertOrUpdate(editedShoppingList);
        realm.commitTransaction();

        Toast.makeText(getApplicationContext(), getText(R.string.savedSuccessfully), Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    public void buttonAddProduct(View view)
    {
        CustomAddProductTile customAddProductTile = new CustomAddProductTile(this);
        customAddProductTile.Builder();
        linearLayoutEditedProducts.addView(customAddProductTile);
        linearLayoutEditedProducts.refreshDrawableState();
    }
}
