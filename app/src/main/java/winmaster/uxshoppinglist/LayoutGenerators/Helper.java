package winmaster.uxshoppinglist.LayoutGenerators;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import winmaster.uxshoppinglist.R;

class Helper
{
    static Drawable arrangeRandomIcoImage(Context ctx)
    {
        List<Drawable> drawables = new ArrayList<>();
        drawables.add(ContextCompat.getDrawable(ctx, R.drawable.apple));
        drawables.add(ContextCompat.getDrawable(ctx, R.drawable.banana));
        drawables.add(ContextCompat.getDrawable(ctx, R.drawable.broccoli));
        drawables.add(ContextCompat.getDrawable(ctx, R.drawable.lemon));
        drawables.add(ContextCompat.getDrawable(ctx, R.drawable.orange));
        drawables.add(ContextCompat.getDrawable(ctx, R.drawable.pepper));
        drawables.add(ContextCompat.getDrawable(ctx, R.drawable.strawberry));
        drawables.add(ContextCompat.getDrawable(ctx, R.drawable.kale));
        drawables.add(ContextCompat.getDrawable(ctx, R.drawable.icon_cart));

        Random random = new Random();

        return drawables.get(random.nextInt(drawables.size()));
    }
}
