package winmaster.uxshoppinglist.LayoutGenerators;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.travijuu.numberpicker.library.NumberPicker;

import winmaster.uxshoppinglist.R;

public class CustomAddProductTile extends RelativeLayout
{
    Context context;
    View rootView;
    CardView cardViewContainer;
    EditText editTextProductName;
    ImageView imageViewProduct;
    NumberPicker numberPicker;

    public CustomAddProductTile(Context context)
    {
        super(context);
        this.context = context;
        Init(context);
    }

    private void Init(Context context)
    {
        rootView = inflate(context, R.layout.tile_product_add, this);

        cardViewContainer = rootView.findViewById(R.id.cardViewProductAdd);
        editTextProductName = rootView.findViewById(R.id.editTextProductName);
        imageViewProduct = rootView.findViewById(R.id.imageViewProductAdd);
        numberPicker = rootView.findViewById(R.id.number_picker);
    }

    public void Builder()
    {
        imageViewProduct.setImageDrawable(Helper.arrangeRandomIcoImage(getContext()));
        numberPicker.setMax(15);
        numberPicker.setMin(1);
        numberPicker.setValue(1);
    }

    public void Builder(String productName, int amount)
    {
        imageViewProduct.setImageDrawable(Helper.arrangeRandomIcoImage(getContext()));
        numberPicker.setMax(15);
        numberPicker.setValue(amount);
        editTextProductName.setText(productName);
    }

    public int GetNumberPickerData()
    {
        return numberPicker.getValue();
    }

    public String GetProductNameData()
    {
        return editTextProductName.getText().toString().equals("") ?
        getResources().getString(R.string.productPlaceholder)
        : editTextProductName.getText().toString();
    }
}
