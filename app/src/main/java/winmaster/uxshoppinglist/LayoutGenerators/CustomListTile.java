package winmaster.uxshoppinglist.LayoutGenerators;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;


import winmaster.uxshoppinglist.ListActivity;
import winmaster.uxshoppinglist.R;

public class CustomListTile extends RelativeLayout
{
    Context context;
    View rootView;
    CardView cardViewContainer;
    public TextView textViewListTitle;
    public CheckBox checkBoxListChecked;

    public CustomListTile(Context context)
    {
        super(context);
        this.context = context;
        Init(context);
    }

    private void Init(Context context)
    {
        rootView = inflate(context, R.layout.tile_list, this);

        cardViewContainer = rootView.findViewById(R.id.cardViewList);
        textViewListTitle = rootView.findViewById(R.id.textViewListName);
        checkBoxListChecked = rootView.findViewById(R.id.checkBoxListChecked);
    }

    public void Builder(String title, boolean checked)
    {
        textViewListTitle.setText(title);
        checkBoxListChecked.setChecked(checked);

        cardViewContainer.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(getContext(), ListActivity.class);
                intent.putExtra("LIST_NAME", textViewListTitle.getText().toString());
                context.startActivity(intent);
            }
        });
    }
}
