package winmaster.uxshoppinglist.LayoutGenerators;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import winmaster.uxshoppinglist.R;

public class CustomProductTile extends RelativeLayout
{
    Context context;
    View rootView;
    CardView cardViewContainer;
    public TextView textViewProductName;
    public CheckBox checkBoxProductChecked;
    ImageView imageViewProduct;
    TextView textViewProductAmount;

    public CustomProductTile(Context context)
    {
        super(context);
        this.context = context;
        Init(context);
    }

    private void Init(Context context)
    {
        rootView = inflate(context, R.layout.tile_product_show, this);

        cardViewContainer = rootView.findViewById(R.id.cardViewProduct);;
        textViewProductName = rootView.findViewById(R.id.textViewProductName);
        checkBoxProductChecked = rootView.findViewById(R.id.checkBoxProductChecked);
        imageViewProduct = rootView.findViewById(R.id.imageViewProduct);
        textViewProductAmount = rootView.findViewById(R.id.textViewProductAmount);
    }

    public void Builder(String title, int amount, boolean checked)
    {
        textViewProductName.setText(title);
        checkBoxProductChecked.setChecked(checked);
        textViewProductAmount.setText(String.valueOf(amount));

        imageViewProduct.setImageDrawable(Helper.arrangeRandomIcoImage(getContext()));
    }
}
