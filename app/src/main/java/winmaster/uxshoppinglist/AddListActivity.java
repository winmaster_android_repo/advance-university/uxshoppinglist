package winmaster.uxshoppinglist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.sql.Date;

import io.realm.Realm;
import io.realm.RealmList;
import winmaster.uxshoppinglist.LayoutGenerators.CustomAddProductTile;
import winmaster.uxshoppinglist.Models.Product;
import winmaster.uxshoppinglist.Models.ShoppingList;

public class AddListActivity extends AppCompatActivity
{
    public Realm realm;

    EditText editTextListName;
    LinearLayout linearLayoutNewProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        setContentView(R.layout.activity_add_list);

        editTextListName = findViewById(R.id.editTextListName);
        linearLayoutNewProducts = findViewById(R.id.linearLayoutNewProducts);
    }

    public void buttonBackClicked(View view)
    {
        this.onBackPressed();
    }

    public void buttonSaveListClicked(View view)
    {
        RealmList<Product> products = new RealmList<>();

        for(int i=0; i<linearLayoutNewProducts.getChildCount(); i++)
        {
            CustomAddProductTile child = (CustomAddProductTile)linearLayoutNewProducts.getChildAt(i);
            Product product = new Product(child.GetProductNameData(), child.GetNumberPickerData(), new Date(System.currentTimeMillis()));
            products.add(product);
        }

        String shoppingListTitle = editTextListName.getText().toString().equals("") ? getResources().getString(R.string.addNewListHint) : editTextListName.getText().toString();
        ShoppingList newShoppingList = new ShoppingList(shoppingListTitle, products);

        realm.beginTransaction();
        realm.insertOrUpdate(newShoppingList);
        realm.commitTransaction();

        Toast.makeText(getApplicationContext(), getText(R.string.savedSuccessfully), Toast.LENGTH_SHORT).show();
        this.onBackPressed();
    }

    public void buttonAddProduct(View view)
    {
        CustomAddProductTile customAddProductTile = new CustomAddProductTile(this);
        customAddProductTile.Builder();
        linearLayoutNewProducts.addView(customAddProductTile);
        linearLayoutNewProducts.refreshDrawableState();
    }
}
