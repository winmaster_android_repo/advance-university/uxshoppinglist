package winmaster.uxshoppinglist;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import io.realm.Realm;
import winmaster.uxshoppinglist.Models.SampleDataCreator;

public class App extends Application
{
    Realm realm;

    @Override
    public void onCreate()
    {
        super.onCreate();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (!prefs.getBoolean("firstRun", false))
        {
            databaseSetup();

            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("firstRun", true);
            editor.commit();
        }
    }

    private void databaseSetup()
    {
        Realm.init(getApplicationContext());
        realm = Realm.getDefaultInstance();
        SampleDataCreator.AddSampleData(realm);
    }
}