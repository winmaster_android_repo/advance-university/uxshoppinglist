package winmaster.uxshoppinglist;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import io.realm.Realm;
import io.realm.RealmResults;
import winmaster.uxshoppinglist.LayoutGenerators.CustomListTile;
import winmaster.uxshoppinglist.Models.SampleDataCreator;
import winmaster.uxshoppinglist.Models.ShoppingList;

public class HomeActivity extends AppCompatActivity
{
    public Realm realm;
    LinearLayout linearLayoutLists;
    FloatingActionButton floatingActionButtonDelete;
    String listTitle;
    ShoppingList shoppingList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        linearLayoutLists  = findViewById(R.id.linearLayoutLists);
        floatingActionButtonDelete = findViewById(R.id.floatingActionButtonDelete);

        Realm.init(this);
        realm = Realm.getDefaultInstance();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        PopulateListOnView();
        floatingActionButtonDelete.setVisibility(View.INVISIBLE);
    }

    private void PopulateListOnView()
    {
        linearLayoutLists.removeAllViews();
        RealmResults<ShoppingList> lists = realm.where(ShoppingList.class).findAll();

        for (ShoppingList list: lists)
        {
            final CustomListTile customListTile = new CustomListTile(this);
            customListTile.Builder(list.getName(), false);

            linearLayoutLists.addView(customListTile);
            linearLayoutLists.refreshDrawableState();

            customListTile.checkBoxListChecked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
            {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b)
                {
                    listTitle = customListTile.textViewListTitle.getText().toString();

                    if(compoundButton.isChecked())
                    {
                        floatingActionButtonDelete.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        floatingActionButtonDelete.setVisibility(View.INVISIBLE);
                    }
                }
            });
        }
    }

    public void addNewListClicked(View view)
    {
        startActivity(new Intent(this, AddListActivity.class));
    }

    public void deleteListClicked(View view)
    {
        shoppingList = realm.where(ShoppingList.class).equalTo("name", listTitle).findFirst();

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(R.string.deleteEntry)
                .setMessage(R.string.deleteConfirm)
                .setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        realm.executeTransaction(new Realm.Transaction()
                        {
                            @Override
                            public void execute(Realm realm)
                            {
                                shoppingList.deleteFromRealm();
                            }
                        });

                        Toast.makeText(getApplicationContext(), getText(R.string.deleteSuccess), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                    }
                })
                .setNegativeButton(R.string.No, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .create();

        dialog.show();
    }

    public void onBackPressed()
    {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
