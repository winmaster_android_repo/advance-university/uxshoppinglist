package winmaster.uxshoppinglist;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import winmaster.uxshoppinglist.LayoutGenerators.CustomProductTile;
import winmaster.uxshoppinglist.Models.Product;
import winmaster.uxshoppinglist.Models.ShoppingList;

public class ListActivity extends AppCompatActivity
{
    public Realm realm;
    LinearLayout linearLayoutProducts;
    String listName;
    String productTitle;
    ShoppingList shoppingList;
    List<Product> productList;

    TextView textViewListName;
    FloatingActionButton floatingActionButtonDeleteProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        realm = Realm.getDefaultInstance();

        listName = getIntent().getExtras().getString("LIST_NAME");

        linearLayoutProducts = findViewById(R.id.linearLayoutProducts);
        textViewListName = findViewById(R.id.textViewListName);
        textViewListName.setText(listName);

        floatingActionButtonDeleteProduct = findViewById(R.id.floatingActionButtonDeleteProduct);

        arrangeProductsToList();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        arrangeProductsToList();
        floatingActionButtonDeleteProduct.setVisibility(View.INVISIBLE);
    }

    private void arrangeProductsToList()
    {
        linearLayoutProducts.removeAllViews();
        shoppingList = realm.where(ShoppingList.class).equalTo("name", listName).findFirst();

        productList = shoppingList.getProducts();

        for( Product product: productList)
        {
            final CustomProductTile customProductTile = new CustomProductTile(this);
            customProductTile.Builder(product.getName(), product.getAmount(), false);

            linearLayoutProducts.addView(customProductTile);
            linearLayoutProducts.refreshDrawableState();

            customProductTile.checkBoxProductChecked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
            {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b)
                {
                    productTitle = customProductTile.textViewProductName.getText().toString();

                    if(compoundButton.isChecked())
                    {
                        floatingActionButtonDeleteProduct.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        floatingActionButtonDeleteProduct.setVisibility(View.INVISIBLE);
                    }
                }
            });
        }
    }

    public void buttonBackClicked(View view)
    {
        this.onBackPressed();
    }

    public void buttonEditClicked(View view)
    {
        startActivity(new Intent(this, EditListActivity.class)
                .putExtra("LIST_NAME", listName));
    }

    public void buttonDeleteProductClicked(View view)
    {
        int indexToRemove = 0;

        for(int i = 0; i<productList.size(); i++)
        {
            if(productList.get(i).getName().equals(productTitle))
            {
                indexToRemove = i;
                break;
            }
        }

        realm.beginTransaction();
        productList.remove(indexToRemove);
        realm.commitTransaction();

        finish();
        startActivity(getIntent());
    }
}
